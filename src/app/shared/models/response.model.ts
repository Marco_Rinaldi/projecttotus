export interface ResponseLogin {
	content?: any;
	errorCode: string;
	errorMessage: string;
	message: string;
	messageCode: string;
	status: boolean;
	serviceStatus: boolean;
}

export interface FormaData {
	username: string;
	password: string;
}
