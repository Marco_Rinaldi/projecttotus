import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { ResponseLogin } from '../models/response.model';
import { AuthGuard } from '../../main/login/auth.guard';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class AuthService {
	constructor(
		private httpClient: HttpClient,
		private loadingCtrl: LoadingController,
		private authGuard: AuthGuard,
		private router: Router
	) {}

	loginAuth(formToSend: NgForm) {
		const form = this.convertToDataForm(formToSend);
		this.loadingCtrl
			.create({
				message: 'Loading',
				spinner: 'circles'
			})
			.then(loadingEl => {
				loadingEl.present();
				this.httpClient
					.post<ResponseLogin>('http://totus.dot-env.com/login', form)
					.subscribe(response => {
						if (response.errorCode !== '') {
							return;
						}
						this.authGuard.canLand(response);
						if (this.authGuard.canChange) {
							loadingEl.dismiss();
							this.router.navigateByUrl('/landing');
						}
					});
			});
	}

	// Metodo che converte i dati in formData
	private convertToDataForm(form: NgForm): FormData {
		const formToSend = new FormData();
		for (const key in form.value) {
			if (Object.keys(form.value).length > 0) {
				formToSend.set(key, form.value[key]);
			}
		}
		return formToSend;
	}

	private presentLoading() {}
}
