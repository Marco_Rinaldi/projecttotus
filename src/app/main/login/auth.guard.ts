import { Injectable } from '@angular/core';
import {
	CanLoad,
	Route,
	UrlSegment,
	ActivatedRouteSnapshot,
	RouterStateSnapshot,
	UrlTree
} from '@angular/router';
import { Observable } from 'rxjs';
import { ResponseLogin } from '../../shared/models/response.model';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class AuthGuard implements CanLoad {
	private _canChange = false;
	constructor(private router: Router) {}
	canLoad(
		route: Route,
		segments: UrlSegment[]
	): Observable<boolean> | Promise<boolean> | boolean {
		if (!this._canChange) {
			this.router.navigateByUrl('/login');
		}
		return this._canChange;
	}

	canLand(response: ResponseLogin) {
		if (response.errorCode !== '') {
			this._canChange = false;
		}
		this._canChange = true;
	}

	get canChange() {
		return this._canChange;
	}
}
