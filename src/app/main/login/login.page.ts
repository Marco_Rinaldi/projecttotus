import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AuthService } from '../../shared/services/auth.service';
import { AuthGuard } from './auth.guard';
import { ResponseLogin } from '../../shared/models/response.model';

@Component({
	selector: 'app-login',
	templateUrl: './login.page.html',
	styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
	constructor(
		private authService: AuthService,
	) {}

	ngOnInit() {}

	onSubmit(form: NgForm) {
		this.authService.loginAuth(form);
	}
}
